#This is a test to check if the output types defined  are in the proper format
tst_current <- set_output_type(varnames = "PPT")
rownames(tst_current) <- NULL
tst_target <- data.frame(variable = "PPT", object = "WBclim", name = "PPT", index = as.integer(1), digit = as.integer(2))
expect_identical(tst_current, tst_target)
