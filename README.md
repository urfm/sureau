# About

SurEau-Ecos is a trait-based plant hydraulic model designed to simulate tree dehydration and mortality at scales ranging from stand to region, utilising the R programming language

# Installation

```         
devtools::install_git(url = "https://forgemia.inra.fr/urfm/sureau.git", ref = "main")
```

# Literature

-   Ruffault, J., Pimont, F., Cochard, H., Dupuy, J.-L., Martin-StPaul, N., 2022. SurEau-Ecos v2.0: a trait-based plant hydraulics model for simulations of plant water status and drought-induced mortality at the ecosystem level. Geoscientific Model Development 15, 5593–5626. <https://doi.org/10.5194/gmd-15-5593-2022>

-   Ruffault, J., Limousin, J., Pimont, F., Dupuy, J., De Càceres, M., Cochard, H., Mouillot, F., Blackman, C. J., Torres‐Ruiz, J. M., Parsons, R. A., Moreno, M., Delzon, S., Jansen, S., Olioso, A., Choat, B., & Martin‐StPaul, N. (2023). Plant hydraulic modelling of leaf and canopy fuel moisture content reveals increasing vulnerability of a Mediterranean forest to wildfires under extreme drought. *New Phytologist*, *237*(4), 1256–1269. <https://doi.org/10.1111/nph.18614>



# Getting started 

1/install the version you need with devtools
devtools::install_git(url = "https://forgemia.inra.fr/urfm/sureau.git", ref = "main")
require(sureau)



2/ stteing the initialization objects running sureau (using runSurEau()) function returns the internal list of variables, which is
accessible if the output of the function is assigned to an object. You can also store output easier in a permanent file, set to e. g. file.path(getwd(), "example_output.csv")


3/Set your options, parameters, and boundary conditions
you can use :
- default values using the following example and the change the object. 
- For plant and soil you can also read files containg your parametrisation

progress printer slows down the simulation, so if performance is crucial it is recommended to set printProg=FALSE

sim_opt <- create.modeling.options(printProg = TRUE,
                                   #transpirationModel = "Granier",
                                   lat = 48.73, lon = 6.23,
                                   yearStart = 1990, yearEnd = 1990,
                                   outputResolution = "subdaily",
                                   outputType = "diagnostic",
                                   outputOverwrite = TRUE,
                                   outputPath = NULL)


par_soil <- create.soil.parameters()

par_veg <- create.vegetation.parameters()


For meteo you need to create a path to the file (here an example file)

climateData_path <- example_fpath("climate_example.csv")
dat_meteo <- create.climate.data(filePath = climateData_path,
                                 modeling_options = sim_opt)


Initialize the model objects 

myModel <- initSurEau(opt = sim_opt, parSoil = par_soil, parPlant = par_veg,
                      datMeteo = dat_meteo)
myModel$printDiagnostics()

3/ --- run model

results <- runSurEau(x = myModel,
                     deepWater = FALSE) # Test GR

4/ format the results

results_Formatted = sureau:::convertWB2DF(results)


