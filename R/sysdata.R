# sysdata_output_ref
#' Read output_ref.csv file
#' @return Data frame containing data in output_ref.csv
#' @export
sysdata_output_ref <- function() {
  fpath <- system.file("data-raw", "output", "output_ref.csv",
                       package = "sureauModel", mustWork = TRUE)
  outputref <- utils::read.table(file = fpath, dec = ".", sep = ";", header = T, stringsAsFactors = F)
  if (anyDuplicated(outputref$variable)) stop("Duplicated variable names in output_ref.csv at line(s): ",
                                              paste(which(duplicated(outputref$variable)) + 1, collapse = ", "))
  return(outputref)
}

# sysdata_source_ext
#' Source yet unfinished model functions
#' @return Sourced functions
#' @export
sysdata_source_ext <- function() {
  dpath <- system.file("unfinished_functions", package = "sureauModel", mustWork = TRUE)
  for (i in list.files(path = dpath, pattern = "[.][Rr]$", full.names = TRUE)) {
    source(file = i)
  }
}